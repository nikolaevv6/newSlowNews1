<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Error Registration</title>
  <link rel="stylesheet" href="main.css">

</head>
<body>
<div id="header"><h1>Slooooow News</h1></div>
<div id="sidebar">
  <h2><a href="login.jsp">Login</a></h2>

  <h2><a href="registration.jsp">Registration</a></h2>

  <h2><a href="news.jsp">News</a></h2>

  <h2><a href="archive.jsp">Archive</a></h2>
</div>
<div id="content">

  <h2>Error Registration!</h2><br/>
  <h2>User already exist, or invalid user's data !</h2><br/>
</div>
<div id="footer">&copy; Vova</div>
</body>
</html>
